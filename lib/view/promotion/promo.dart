import 'package:flutter/material.dart';

class PromoMenu extends StatefulWidget {
  _PromoMenuState createState() => _PromoMenuState();
}

class _PromoMenuState extends State<PromoMenu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Produk Promo',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        child: Center(
          child: Text('Maaf, belum ada produk promo'),
        ),
      ),
    );
  }
}
