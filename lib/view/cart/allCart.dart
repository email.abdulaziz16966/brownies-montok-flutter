import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ketixclone/api.dart';
import 'package:ketixclone/models/order.dart';
import 'package:ketixclone/view/checkout/reservation.dart';
import 'package:ketixclone/view/everything/informasi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class AllCartProduct extends StatefulWidget {
  _AllCartProductState createState() => _AllCartProductState();
}

class _AllCartProductState extends State<AllCartProduct> {
  String username, idCustomer;
  final semuaPesanan = List<TotalTransaction>();
  final totalOrders = List<TotalOrders>();
  final oCcy = new NumberFormat("#,##0", "en_US");
  var total;
  var cek = false;
  var isLoading = false;

  _getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString('username');
      idCustomer = preferences.getString('idCustomer');
    });
    _getAllOrder();
    _totalHargaOrder();
  }

  _kurangiTmpProduk(String idMenu) async {
    await http.post(ApiUrl.baseUrl + ApiUrl.deleteTmpProduk,
        body: {'username': username, 'Menu_ID': idMenu});
    setState(() {
      _getAllOrder();
      _totalHargaOrder();
    });
  }

  updateQuantity(String angka, String idMenu) async {
    final response = await http.post(ApiUrl.baseUrl + ApiUrl.updateQtyTransaksi,
        body: {"Menu_ID": idMenu, "qty": angka, "username": username});
    final data = jsonDecode(response.body);
    int value = data['value'];

    if (value == 1) {
      setState(() {
        _getAllOrder();
        _totalHargaOrder();
      });
    }
  }

  _getAllOrder() async {
    semuaPesanan.clear();
    setState(() {
      isLoading = true;
    });
    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.allTmpTransaksi + username);

    if (responses.contentLength == 2) {
      setState(() {
        cek = false;
      });
    } else {
      final getData = jsonDecode(responses.body);
      getData.forEach((api) {
        final all = new TotalTransaction(
            api['id'],
            api['Menu_ID'],
            api['Menu_name'],
            api['Quantity'],
            api['Price'],
            api['Weight'],
            api['Tmp_image'],
            api['idCustomer'],
            api['username'],
            api['total']);
        semuaPesanan.add(all);
      });
      setState(() {
        cek = true;
      });
    }
  }

  _totalHargaOrder() async {
    totalOrders.clear();
    setState(() {
      isLoading = true;
    });

    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.sumTmpTransaksi + username);
    final getData = jsonDecode(responses.body);
    getData.forEach((api) {
      final totalI = new TotalOrders(api['total']);
      totalOrders.add(totalI);
      final saveTotal = totalI.total;
      setState(() {
        _simpanTotalPesanan(saveTotal);
      });
    });
    setState(() {
      isLoading = false;
    });
  }

  _simpanTotalPesanan(String totalPesenan) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString('totalPesenan', totalPesenan);
      preferences.commit();
    });
  }

  _submitHapus() async {
    final response = await http.post(
        ApiUrl.baseUrl + ApiUrl.hapusSemuaTransaksi,
        body: {"username": username, "idCustomer": idCustomer});
    final data = jsonDecode(response.body);
    int value = data['value'];
    if (value == 1) {
      setState(() {
        _getAllOrder();
        _totalHargaOrder();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1.0,
        centerTitle: true,
        title: Text(
          'Order Details',
          style: TextStyle(color: Colors.black),
        ),
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: GestureDetector(
              onTap: () {
                if (cek == true) {
                  _hapusSemuaPesanan();
                } else {
                  _hapusOrderDuluGaes();
                }
              },
              child: Icon(Icons.delete, color: Colors.black),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => BagianInformasi()));
              },
              child: Icon(Icons.help, color: Colors.black),
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: totalOrders.length,
                      itemBuilder: (BuildContext context, int index) {
                        final to = totalOrders[index];
                        return Center(
                          child: cek
                              ? Text(
                                  'Total : Rp. ' +
                                      oCcy.format(int.parse(to.total)) +
                                      '.-',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                )
                              : Text(
                                  'Total : Rp. 0.-',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                        );
                      })),
            ),
            Expanded(
              child: Material(
                color: Colors.brown,
                child: MaterialButton(
                  onPressed: () {
                    if (cek == true) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ReservationOrders()));
                    } else {
                      _orderDuluGaes();
                    }
                  },
                  child:
                      Text('Checkout', style: TextStyle(color: Colors.white)),
                ),
              ),
            )
          ],
        ),
      ),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  cek
                      ? ListView.builder(
                          padding: EdgeInsets.all(8.0),
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: semuaPesanan.length,
                          itemBuilder: (context, index) {
                            final sp = semuaPesanan[index];
                            return ListView(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, top: 8.0),
                                  child: Text(
                                    sp.menuName,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Container(
                                        child: Image.network(
                                          ApiUrl.baseUrl + sp.tmpImage,
                                          height: 75.0,
                                          width: 100.0,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    VerticalDivider(),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Column(
                                                children: <Widget>[
                                                  Text(
                                                    'Qty',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text(sp.quantity)
                                                ],
                                              ),
                                              VerticalDivider(
                                                color: Colors.black,
                                              ),
                                              Column(
                                                children: <Widget>[
                                                  Text(
                                                    'Berat',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text(sp.weight + ' Kg')
                                                ],
                                              ),
                                              VerticalDivider(
                                                color: Colors.black,
                                              ),
                                              Column(
                                                children: <Widget>[
                                                  Text(
                                                    'Harga Satuan',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text('Rp. ' +
                                                      oCcy.format(
                                                          int.parse(sp.price)) +
                                                      '.-')
                                                ],
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              OutlineButton(
                                                onPressed: () {
                                                  _hapusItemPesanan(
                                                      sp.menuName, sp.idMenu);
                                                },
                                                child: Text('Remove'),
                                              ),
                                              VerticalDivider(),
                                              OutlineButton(
                                                onPressed: () {
                                                  _editQuantity(sp.idMenu);
                                                },
                                                child: Text('Edit'),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Divider(),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text('Sub Total'),
                                      Text(
                                        'Rp. ' +
                                            oCcy.format(int.parse(sp.total)) +
                                            '.-',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(),
                              ],
                            );
                          },
                        )
                      : _noAddtoCart(),
                  cek
                      ? ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: totalOrders.length,
                          itemBuilder: (BuildContext context, int index) {
                            final to = totalOrders[index];
                            return ListView(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.only(left: 10.0, right: 10.0),
                              children: <Widget>[
                                SizedBox(
                                  height: 16.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Jumlah',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Rp. ' +
                                          oCcy.format(int.parse(to.total)) +
                                          '.-',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Ongkos kirim',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Free',
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'PPN 0%',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Rp. 0,-',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                SizedBox(
                                  height: 16.0,
                                )
                              ],
                            );
                          },
                        )
                      : SizedBox(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<bool> _orderDuluGaes() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text('Pilih produk terlebih dahulu.'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                            },
                            child: Text('OK'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }

  Future<bool> _hapusOrderDuluGaes() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text('Tidak Ada Pesanan Tersedia.'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                            },
                            child: Text('OK'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }

  _editQuantity(String idMenu) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  color: Colors.black,
                  child: Text(
                    'Ubah Qty Pesanan',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 8.0),
                  child: _editPesanan(),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: SizedBox(),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          setState(() {
                            updateQuantity(angka.toString(), idMenu);
                          });
                        },
                        child: Text('OK'),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  int angka = 1;
  Widget _editPesanan() {
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            angka == 1
                ? OutlineButton(
                    padding: EdgeInsets.all(5.0),
                    onPressed: () {},
                    child: Text(
                      '-',
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                : OutlineButton(
                    padding: EdgeInsets.all(5.0),
                    onPressed: () {
                      setState(() {
                        angka--;
                        //total = int.parse(widget.items.price) * angka;
                      });
                    },
                    child: Text(
                      '-',
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
            SizedBox(
              width: 5.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.grey[300])),
              child: Text(
                '$angka',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              width: 5.0,
            ),
            OutlineButton(
              onPressed: () {
                setState(() {
                  angka++;
                  //total = int.parse(widget.items.price) * angka;
                });
              },
              child: Text(
                '+',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _noAddtoCart() {
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 150.0),
      children: <Widget>[
        Center(
          child: Image.asset(
            './img/no_order.png',
            height: 200.0,
            width: 150.0,
          ),
        ),
        Center(
          child: Text('Tidak Ada Pesanan Tersedia',
              style: TextStyle(
                fontSize: 20.0,
              )),
        ),
      ],
    );
  }

  Future<bool> _hapusSemuaPesanan() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text('Yakin ingin hapus semua pesanan?'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Text('No'),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              setState(() {
                                _submitHapus();
                              });
                            },
                            child: Text('Yes'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }

  Future<bool> _hapusItemPesanan(String menuName, String idMenu) {
    print('$idMenu');
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child:
                          Text('Kamu yakin ingin hapus pesanan \n$menuName?'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Text('No'),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              setState(() {
                                _kurangiTmpProduk(idMenu);
                              });
                            },
                            child: Text('Yes'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }
}
