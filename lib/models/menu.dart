class MenuItems {
  final String idMenu;
  final String nameMenu;
  final String price;
  final String weight;
  final String stock;
  final String imageMenu;
  final String description;
  final String quantity;

  MenuItems(
    this.idMenu, 
    this.nameMenu, 
    this.price, 
    this.weight,
    this.stock, 
    this.imageMenu, 
    this.description, 
    this.quantity);
}
