class TransactionModels {
  final String id;
  final String invoice;
  final String tglTransaksi;
  final String status;
  final String idCustomer;
  final String username;
  final String nama;

  TransactionModels(this.id, this.invoice, this.tglTransaksi, this.status,
      this.idCustomer, this.username, this.nama);
}

class DetailProdukTransaksiModels {
  final String id;
  final String idTransaksi;
  final String menuId;
  final String quantity;
  final String price;
  final String menuName;
  final String menuImage;
  final String menuWeight;
  final String total;

  DetailProdukTransaksiModels(
      this.id,
      this.idTransaksi,
      this.menuId,
      this.quantity,
      this.price,
      this.menuName,
      this.menuImage,
      this.menuWeight,
      this.total);
}

class SumDetailTotalTransaksi {
  final String total;

  SumDetailTotalTransaksi(this.total);
}
